#!/usr/bin/env python3
import argparse

import sys
from os.path import dirname, realpath
p = dirname(realpath(__file__))
sys.path.append(p)
from preproc.preprocessor import PreProcessor, PreProcessorError, is_importable_file
import logging

parser = argparse.ArgumentParser(description="Is normal MiniJava file?", add_help=True)
parser.add_argument("file", metavar="FILE", help="File to check")
file = parser.parse_args().file

try:
    ret = is_importable_file(file)
    sys.exit(1 if ret else 0)
except PreProcessorError as ex:
    logging.exception("")
    sys.exit(1)