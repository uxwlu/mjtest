#!/usr/bin/env python3

import sys
from os.path import dirname, realpath
p = dirname(realpath(__file__))
sys.path.append(p)
from preproc.cli import run
run()