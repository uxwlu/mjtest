MiniJava preprocessor
=====================

Why?
----
It allows to create a library of utility classes and reduces the overall code completion in the test file.
As standard java import and package statements are used, the unprocessed files are valid java files (with all the advantages in the development process).

Usage
-----

```
usage: process.py [-h] [--log_level LOG_LEVEL] FILE MJ_IMPORT_DIR DEST

MiniJava preprocessor

positional arguments:
  FILE                  File to pre process
  MJ_IMPORT_DIR         Base directory for imports. Can be omitted by
                        assigning the environment variable MJ_IMPORT_DIR.
  DEST                  Destination file, '-' if it should be printed on std
                        out.

optional arguments:
  -h, --help            show this help message and exit
  --log_level LOG_LEVEL
                        Logging level (error, warn, info or debug)
```

Importable file
---------------

A importable file "X.java" contains at least:

- a `package` statement
- the string `public class X` somewhere but not in a comment (this isn't checked automatically)

It can import other importable files.

Check for "normal" mj files via `./is_normal_mj_file.py FILE`.

What `import x.y.Math` does:
----------------------------

1. Find file `MJ_IMPORT_DIR/x/y/Math.java` and check that it's an importable file
2. Check that the file isn't already imported (ignore it otherwise) and abort if two imported classes are in different packages but have the same name.
3. Store the file in a list of files that should be imported
4. Start with 1. for each `import` statement in the file that isn't already satisfied
5. For each file that should be imported:
	1. Read the file
	2. Remove all strings that match `package.*;`
	3. Replace all `public class` strings with just `class`
	4. Insert it at the end of the original source file
6. Output the modified source file