import shutil
from mjtest.environment import Environment, TestMode, TEST_MODES, get_test_dirname
from mjtest.test.tests import TestCase, BasicTestResult
from os import path

from mjtest.util.utils import decode


class BasicSyntaxTest(TestCase):

    FILE_ENDINGS = [".invalid.mj", ".valid.mj", ".mj", ".invalid.java", ".java"]
    MODE = TestMode.syntax

    def __init__(self, env: Environment, type: str, file: str, preprocessed_file: str, log_file_mode: str):
        super().__init__(env, type, file, preprocessed_file, log_file_mode)
        if type != self.MODE and TEST_MODES.index(type) > TEST_MODES.index(self.MODE):
            self._should_succeed = True
        else:
            self._should_succeed = not file.endswith(".invalid.mj") and not file.endswith(".invalid.java")

    def should_succeed(self) -> bool:
        return self._should_succeed

    def short_name(self) -> str:
        test_dir = None
        modes = [self.MODE]
        if self.MODE in TestMode.USE_TESTS_OF_OTHER:
            modes += TestMode.USE_TESTS_OF_OTHER[self.MODE]
        for mode in modes:
            subdir = path.join(self.env.test_dir, get_test_dirname(mode))
            if self.file.startswith(subdir):
                test_dir = subdir
                break
        if test_dir is None:
            return path.relpath(self.file, self.env.test_dir)
        return path.relpath(self.file, test_dir)

    def run(self) -> BasicTestResult:
        out, err, rtcode = self.env.run_mj_command(self.MODE, self.preprocessed_file)
        return BasicTestResult(self, rtcode, decode(out), decode(err))

TestCase.TEST_CASE_CLASSES[TestMode.syntax].append(BasicSyntaxTest)


class JavaCompileTest(BasicSyntaxTest):
    """
    The MiniJava compiler should behave the same as javac
    """

    FILE_ENDINGS = [".java"]
    SYNTAX_TEST = True

    def __init__(self, env: Environment, type: str, file: str, preprocessed_file: str, log_file_mode: str):
        super().__init__(env, type, file, preprocessed_file, log_file_mode)
        tmp_dir = self.env.create_pid_local_tmpdir()
        _, self.javac_err, self.javac_rtcode = \
            self.env.run_command("javac", path.relpath(preprocessed_file), "-d", tmp_dir, "-verbose")
        self.javac_err = decode(self.javac_err)  # type: str
        self._should_succeed = self._is_file_syntactically_correct() if self.SYNTAX_TEST else self.javac_rtcode == 0

    def _is_file_syntactically_correct(self):
        return self.javac_rtcode == 0 or "META-INF" in self.javac_err

    def _is_file_semantically_correct(self):
        return self.javac_rtcode == 0

    def run(self) -> BasicTestResult:
        ret = super().run()
        ret.add_additional_text("javac (verbose) error output", self.javac_err)
        ret.add_additional_text_line("javac return code", str(self.javac_rtcode))
        ret.add_additional_text_line("Is syntax correct? ", str(self._is_file_syntactically_correct()))
        return ret

#TestCase.TEST_CASE_CLASSES[TestMode.syntax].append(JavaCompileTest)
