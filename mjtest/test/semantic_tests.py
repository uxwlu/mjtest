from mjtest.environment import TestMode
from mjtest.test.syntax_tests import BasicSyntaxTest
from mjtest.test.tests import TestCase


class BasicSemanticTest(BasicSyntaxTest):

    FILE_ENDINGS = [".invalid.mj", ".valid.mj", ".mj", ".invalid.java", ".java"]
    MODE = TestMode.semantic


TestCase.TEST_CASE_CLASSES[TestMode.semantic].append(BasicSemanticTest)