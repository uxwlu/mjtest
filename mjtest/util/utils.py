import logging
from os import path
import sys
from typing import Tuple, Optional, Any, List, Callable
import re

COLOR_OUTPUT_IF_POSSIBLE = False

if sys.stdout.isatty():
    try:
        import termcolor
        COLOR_OUTPUT_IF_POSSIBLE = True
    except ImportError:
        COLOR_OUTPUT_IF_POSSIBLE = False


def force_colored_output():
    global COLOR_OUTPUT_IF_POSSIBLE
    COLOR_OUTPUT_IF_POSSIBLE = True


def get_mjtest_basedir() -> str:
    return path.dirname(path.dirname(path.dirname(path.realpath(__file__))))


def colored(text: str, *args, **kwargs):
    """
    Wrapper around termcolor.colored (if it's loadable)
    """
    global COLOR_OUTPUT_IF_POSSIBLE
    if COLOR_OUTPUT_IF_POSSIBLE:
        return termcolor.colored(text, *args, **kwargs)
    else:
        return text


def cprint(text: str, *args, **kwargs):
    """
    Wrapper around termcolor.cprint (if it's loadable)
    """
    global COLOR_OUTPUT_IF_POSSIBLE
    if COLOR_OUTPUT_IF_POSSIBLE:
        termcolor.cprint(text, *args, **kwargs)
    else:
        print(text)


def get_main_class_name(file: str) -> Optional[str]:
    current_class = None
    with open(file, "r", errors="backslashreplace") as f:
        for line in f:
            if line.startswith("class ") or line.startswith("/*public*/ class "):
                match = re.search("[A-Za-z_0-9]+", line.replace("class ", "").replace("/*public*/", ""))
                if match:
                    has_public_class = True
                    current_class = match.group(0)
            elif "String[]" in line and "main" in line and "void" in line and "static" in line and "public" in line:
                return current_class
    return None


class InsertionTimeOrderedDict:
    """
    A dictionary which's elements are sorted by their insertion time.
    """

    def __init__(self):
        self._dict = {}
        self._keys = []
        dict()

    def __delitem__(self, key):
        """ Remove the entry with the passed key """
        del(self._dict[key])
        del(self._keys[self._keys.index(key)])

    def __getitem__(self, key):
        """ Get the entry with the passed key """
        return self._dict[key]

    def __setitem__(self, key, value):
        """ Set the value of the item with the passed key """
        if key not in self._dict:
            self._keys.append(key)
        self._dict[key] = value

    def __iter__(self):
        """ Iterate over all keys """
        return self._keys.__iter__()

    def values(self) -> List:
        """ Rerturns all values of this dictionary. They are sorted by their insertion time. """
        return [self._dict[key] for key in self._keys]

    def keys(self) -> List:
        """ Returns all keys of this dictionary. They are sorted by their insertion time. """
        return self._keys

    def __len__(self):
        """ Returns the number of items in this dictionary """
        return len(self._keys)

    @classmethod
    def from_list(cls, items: Optional[list], key_func: Callable[[Any], Any]) -> 'InsertionTimeOrderedDict':
        """
        Creates an ordered dict out of a list of elements.
        :param items: list of elements
        :param key_func: function that returns a key for each passed list element
        :return: created ordered dict with the elements in the same order as in the passed list
        """
        if items is None:
            return InsertionTimeOrderedDict()
        ret = InsertionTimeOrderedDict()
        for item in items:
            ret[key_func(item)] = item
        return ret


def decode(arr: bytes) -> str:
    """
    Decodes the passed byte array as UTF8 and handles invalid characters
    """
    return arr.decode("utf-8", "backslashreplace")