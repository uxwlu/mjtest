# source: https://github.com/libfirm/sisyphus, but slightly modified to support Windows

"""
Convenience function
Alternative to subprocess and os.system
"""
import os
import shlex
import subprocess

import errno

has_resource_module = True
try:
    import resource
except ImportError:
    has_resource_module = False
    pass

import sys
import signal
import threading

import logging
_LOG = logging.getLogger("sisyphus")

_EXIT_CODES = dict((-k, v) for v, k in signal.__dict__.items() if v.startswith('SIG'))
del _EXIT_CODES[0]


class SigKill(Exception):
    def __init__(self, retcode, name):
        self.retcode = retcode
        self.name = name


def _lower_rlimit(res, limit):
    if not has_resource_module:
        return
    (soft, hard) = resource.getrlimit(res)
    if soft > limit or soft == resource.RLIM_INFINITY:
        soft = limit
    if hard > limit or hard == resource.RLIM_INFINITY:
        hard = limit
    resource.setrlimit(res, (soft, hard))


class _Execute(object):
    def __init__(self, cmd, timeout, env, rlimit, input_bytes):
        self.cmd       = cmd
        self.timeout   = timeout
        self.env       = env
        self.proc      = None
        self.exception = None
        self.rlimit    = rlimit
        self.input_bytes = input_bytes
        MB = 1024 * 1024
        if not 'RLIMIT_CORE' in rlimit:
            rlimit['RLIMIT_CORE'] = 0
        if not 'RLIMIT_DATA' in rlimit:
            rlimit['RLIMIT_DATA'] = 8192 * MB
        if not 'RLIMIT_STACK' in rlimit:
            rlimit['RLIMIT_STACK'] = 1024 * MB
        if not 'RLIMIT_FSIZE' in rlimit:
            rlimit['RLIMIT_FSIZE'] = 512 * MB

    def _set_rlimit(self):
        if not has_resource_module:
            return
        if self.timeout > 0.0:
            _lower_rlimit(resource.RLIMIT_CPU, self.timeout)
        for k,v in self.rlimit.items():
            _lower_rlimit(getattr(resource,k), v)

    def _run_process(self):
        try:
            prexec_args = {}
            if has_resource_module:
                prexec_args["preexec_fn"] = self._set_rlimit
            self.proc = subprocess.Popen(self.cmd,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE,
                                         stdin=subprocess.PIPE if self.input_bytes else None,
                                         env=self.env,
                                         shell=True,
                                         **prexec_args)
            #if self.input_bytes:
            #    self.proc.stdin.write(self.input_bytes.decode)
            x = self.proc.communicate(input=self.input_bytes, timeout=self.timeout if self.timeout > 0.0 else None)
            self.out, self.err = x
            self.returncode = self.proc.returncode
        except subprocess.TimeoutExpired as t:
            self.returncode = -errno.ETIMEDOUT
            raise SigKill(signal.SIGXCPU, "SIGXCPU")
        #except Exception as e:
        #    self.exception = e

    def run(self):
        self._run_process()

        # Usually python can recognize application terminations triggered by
        # signals, but somehow it doesn't do this for java (I suspect, that java
        # catches the signals itself but then shuts down more 'cleanly' than it
        # should. Calculate to python convention returncode
        if self.returncode > 127:
            self.returncode = 128 - self.returncode
        if self.returncode in _EXIT_CODES or self.returncode == -errno.ETIMEDOUT:
            _LOG.debug("returncode %d recognized as '%s'" % (self.returncode, _EXIT_CODES[self.returncode]))
            raise SigKill(-self.returncode, _EXIT_CODES[self.returncode] + ": " + os.strerror(-self.returncode))
        return (self.out, self.err, self.returncode)

def execute(cmd, env=None, timeout=0, rlimit=None, propagate_sigint=True, input_bytes = None):
    """Execute a command and return stderr and stdout data"""
    if not rlimit:
        rlimit = dict()
    if cmd is str:
        pass
        #cmd = filter(lambda x: x, cmd.split(' '))
    else:
        #cmd = shlex.split(cmd[0]) + cmd[1:]
        cmd = " ".join(shlex.quote(c) for c in cmd)
    exc = _Execute(cmd, timeout, env, rlimit, input_bytes)
    (out, err, returncode) = exc.run()
    if returncode == -signal.SIGINT:
        raise KeyboardInterrupt
    return (out, err, returncode)


if __name__ == "__main__":
    out, err, retcode = execute("hostname")
    print (out)
