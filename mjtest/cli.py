import logging
import os
import sys
import argparse
from typing import Dict
from datetime import datetime
from mjtest.environment import TestMode, Environment, TEST_MODES
from mjtest.test.tests import TestSuite

# adapted from http://stackoverflow.com/a/8527629
from mjtest.util.utils import cprint, colored


class LogLevelChoices(argparse.Action):
    CHOICES = ["error", "warn", "info", "debug"]
    def __call__(self, parser, namespace, value, option_string=None):
        if value:
            if value not in self.CHOICES:
                message = ("invalid choice: {0!r} (choose from {1})"
                           .format(value,
                                   ', '.join([repr(action)
                                              for action in self.CHOICES])))

                raise argparse.ArgumentError(self, message)
            setattr(namespace, self.dest, value)

all_modes = [TestMode.lexer, TestMode.syntax, TestMode.ast, TestMode.semantic,
    TestMode.compile_firm_only, TestMode.compile_only,
    TestMode.compile_firm, TestMode.compile]

class CLI:
    """
    Takes care of running multiple test modes and collecting the total number of run/failed tests
    """

    # Total # of run tests
    count = 0
    # Total # of failed tests (<= self.count)
    failed = 0

    def __init__(self, raw_args):
        if "all" in raw_args["mode"]:
            if len(raw_args["mode"]) != 1:
                print(colored("\"all\" supplied, simply running all tests.", "yellow"))
            modes = all_modes
            # The user asked for all tests to be run, so we should also run big tests
            raw_args["all_exec_tests"] = True
        else:
            modes = raw_args["mode"]

        self.raw_args = raw_args
        self.modes = self.replace_and_warn_legacy_modes(modes)

    def replace_and_warn_legacy_modes(self, modes):
        new_modes = []
        for mode in modes:
            if mode in ["exec", "exec-firm"]:
                new_mode = mode.replace("exec", "compile")
                print(colored("\"{}\" is just an alias for \"{}\", using the latter instead"
                                .format(mode, new_mode),
                              "yellow"))
                new_modes.append(new_mode)
            else:
                new_modes.append(mode)
        return new_modes

    def run_all(self):
        self.cwd = os.path.realpath(".")
        self.report_subdir = datetime.now().strftime("%d-%m-%y_%H-%M-%S")


        if len(self.modes) == 1:
            self.run_mode(self.modes[0])
        else:
            for mode in self.modes:
                self.run_mode(mode)

            if self.failed > 0:  # some tests failed
                print(colored("Ran {} tests, of which ".format(self.count), "red") +
                      colored("{} failed.".format(self.failed), "red", attrs=["bold"]))
            else:
                cprint("All {} run tests succeeded".format(self.count), "green")

    def run_mode(self, mode):
        os.chdir(self.cwd)
        args = self.raw_args.copy()
        args["mode"] = mode
        args["report_subdir"] = self.report_subdir + "_" + mode
        cprint("Running {} tests".format(mode), attrs=["bold"])
        self.run(args)

    def run(self, args: Dict[str, str]):
        suite = TestSuite(Environment(**args))
        ret = None
        try:
            ret = suite.run()
        finally:
            suite.env.clean_up()
            suite.store()
        if ret is not None:
            self.failed += ret.failed
            self.count += ret.count

if True:#__name__ == '__main__':
    parser = argparse.ArgumentParser(description="MiniJava test runner", add_help=True)
    parser.add_argument("mode", nargs="+", # May appear multiple times
                        choices=["all"] + TEST_MODES + ["exec-firm"],
                        help="What do you want to test?")
    if os.getenv("MJ_RUN", None) is None:
        parser.add_argument("mj_run",
                            metavar="MJ_RUN",
                            help="Command to run your MiniJava implementation, e.g. `mj/run`, "
                                 "can be omitted by assigning the environment variable MJ_RUN")
    #parser.add_argument("--tmp_dir", action="store_const", default="", const="tmp_dir",
    #                    help="Used temporary directory")
    #parser.add_argument("--test_dir", action="store_const", default="", const="test_dir",
    #                    help="Directory that contains all test cases, default is the 'tests' directory")
    parser.add_argument("--only_incorrect_tests", action="store_true", default=False,
                        help="Only run the tests that were incorrect the last run")
    parser.add_argument("--all_exec_tests", action="store_true", default=False,
                        help="Run all exec (compile-firm...) tests, not only the small ones")
    parser.add_argument("--produce_no_reports", action="store_true", default=False,
                        help="Produce no long reports besides the command line output")
    parser.add_argument("--produce_all_reports", action="store_true", default=False,
                        help="Produce reports also for correct test cases")
    parser.add_argument("--parallel", action="store_true", default=False,
                        help="Run the tests in parallel")
    parser.add_argument("--output_no_incorrect_reports", action="store_true", default=False,
                        help="Output the long report for every incorrect test case")
    parser.add_argument("--color", action="store_true", default=False,
                        help="Output color output even if the output goes to into a file")
    parser.add_argument("--ci_testing", action="store_true", default=False,
                        help="In mode X the succeeding test cases of later modes/phases should also succeed in "
                             "this mode, and failing test cases of prior modes/phases should also fail in this phase.")
    parser.add_argument("--bench_compiler_flag_1", action="store", default="", type=str,
                        help="Set the first compiler flag/mode that is used for comparison, "
                             "'javac' for the java compiler")
    parser.add_argument("--bench_compiler_flag_2", action="store", default="", type=str,
                        help="Set the first compiler flag/mode that is used for comparison, "
                             "'javac' for the java compiler")
    parser.add_argument("--bench_runs", action="store", type=int, default=10,
                        help="Number of times to run a benchmarked code")
    #parser.add_argument("--timeout", action="store_const", default=30, const="timeout",
    #                    help="Abort a program after TIMEOUT seconds")
    #parser.add_argument("--report_dir", action="store_const", default="", const="report_dir",
    #                    help="Directory to store the reports in, default is 'reports'")
    parser.add_argument("--log_level", action=LogLevelChoices, default="warn", const="log_level",
                        help="Logging level (error, warn, info or debug)")
    args = vars(parser.parse_args())

    if os.getenv("MJ_RUN", None) is not None:
        args["mj_run"] = os.getenv("MJ_RUN")

    cli = CLI(args)
    cli.run_all()

    sys.exit(1 if cli.failed > 0 else 0)
