from distutils.core import setup
from mjtest import VERSION

setup(name='mjtest',
      description="test runner for MiniJava compilers",
      version=VERSION,
      author="Johannes Bechberger",
      author_email="me@mostlynerdless.de",
      packages=[
          'mjtest',
      ],
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'Operating System :: Unix',
          'Programming Language :: Python :: 3.4',
          "Programming Language :: Python :: 3.5",
          "Programming Language :: Python :: 3 :: Only",
          'Topic :: Software Development :: Testing',
      ],
      platforms=['any'],
      licence="MIT",
      entry_points='''
        [console_scripts]
        mjtest=mjtest.cli
    '''
      )
